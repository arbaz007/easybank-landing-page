let hamburgerBtn = document.getElementById("hamburger-btn");
let closeBtn = document.getElementById("close-btn");
let navItems = document.getElementById("nav-items");
let afterEffect = document.getElementById("after-effect")
let flag = true;
hamburgerBtn.addEventListener("click", () => {
  if (flag) {
    hamburgerBtn.classList.add("none");
    hamburgerBtn.classList.remove("hamburger-btn");
    closeBtn.classList.remove("close-btn");
    closeBtn.classList.add("block");
    navItems.classList.remove ("tr-0")
    afterEffect.classList.remove ("tr-0")
    flag = false;
  }
});
closeBtn.addEventListener("click", () => {
  if (!flag) {
    closeBtn.classList.add("close-btn");
    closeBtn.classList.remove("block");
    hamburgerBtn.classList.add("hamburger-btn");
    hamburgerBtn.classList.remove("none");
    navItems.classList.add ("tr-0")
    afterEffect.classList.add ("tr-0")
    flag = true;
  }
});

/* function handleResize() {
    if (window.innerWidth >= 575) {
        closeBtn.style.display = 'none';
        hamburgerBtn.style.display = "none"
    }else {
        closeBtn.style.display = 'block';
        hamburgerBtn.style.display = "block"
    }
}
window.onresize = handleResize; */
